# FengPear OAuth

#### 介绍
这是一个非常实用的oauth登录程序。根据这个程序，可以实现多个软件共用一个账号。
没有安装程序，使用请自行修改数据库信息。
目前为网页型授权登录页面。
调用方式：https://server/login
返回值：[uid]/false(账号或密码错误)

#### 软件架构
login文件夹：登录网页
register文件夹：注册网页

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request